#!/bin/sh
# igep-generate-release - generate release package
#
# Copyright (C) 2019 - IATEC 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# AUTHORS
# Manel Caro mcaro [at] iatec.biz

set -e
# set -x

unset DISTRO_RELEASE

DIR=$PWD
TMPDIR="$PWD/tmp-workdir"
YOCTOPATH=/home/isee/yocto-project
YOCTO_SOPA_PATH=$YOCTOPATH/build/tmp/deploy/images/sopa0000
IMAGE_FILE=igep-minimal-image-sopa0000.tar.bz2
MLO_FILE=MLO
U_BOOT_FILE=u-boot.img
KERNEL_FILE=zImage
KERNEL_DTB=zImage-am335x-sopa0000.dtb
KERNEL_MODULES=modules-sopa0000.tgz


am335x_sopa_copy_bin(){
	echo "uncompress rootfs image ...."
	pv $YOCTO_SOPA_PATH/${IMAGE_FILE} | tar -xj -C ${TMPDIR}
	echo "deploy ...."
	echo $MLO_FILE
	cp $YOCTO_SOPA_PATH/$MLO_FILE $TMPDIR/boot/
	echo $U_BOOT_FILE
	cp $YOCTO_SOPA_PATH/$U_BOOT_FILE $TMPDIR/boot/
	echo $KERNEL_FILE
	cp $YOCTO_SOPA_PATH/$KERNEL_FILE $TMPDIR/boot/
	echo $KERNEL_DTB
	cp $YOCTO_SOPA_PATH/$KERNEL_DTB $TMPDIR/boot/am335x-sopa0000.dtb
	echo $KERNEL_MODULES
	tar xfz $YOCTO_SOPA_PATH/$KERNEL_MODULES -C ${TMPDIR}

}

am335x_sopa_generate_tar_bz2(){
	
	echo "compress rootfs image ...."
	if [ -z $1 ]; then
		BUILD_FILENAME=am335x-sopa.tar.bz2
	else
		BUILD_FILENAME=am335x-sopa-$DISTRO_RELEASE.tar.bz2
	fi
	tar cf - -C $TMPDIR . -P | pv -s $(du -sb $TMPDIR | awk '{print $1}') | bzip2 > $BUILD_FILENAME
}

remove_working_dir(){
	if [ -d "$TMPDIR" ]; then
		echo "Remove Working directory"
		rm -rf $TMPDIR
	fi	
}

prepare_working_dir(){
	echo "Preparing Working directory"
	remove_working_dir
	mkdir -p "$TMPDIR" 
}


# Usage help
usage() {
    echo "usage: $(basename $0) --image <filename>"
cat <<EOF

required options:

--distro <name>
    select distro

Additional/Optional options:
-h --help
    this help

--list
    show distro list

EOF
exit
}

checkparm() {
    if [ "$(echo $1|grep ^'\-')" ];then
        echo "Error: Need an argument"
        usage
    fi
}

show_distro_list(){
	echo "distro list";
	echo "sopa";
}

# parse commandline options
while [ ! -z "$1" ]; do
        case $1 in
                -h|--help)
                        usage
                        ;;
                -l|--list)
                        show_distro_list
                        ;;                        
                -d|--distro)
						checkparm $2
                        DISTRO="$2"
						;;
                -r|--rel)
						checkparm $2
                        DISTRO_RELEASE="$2"
						;;						
        esac
        shift
done

prepare_working_dir

if [ "$DISTRO" = "sopa" ]; then	
	am335x_sopa_copy_bin $DISTRO_RELEASE
	am335x_sopa_generate_tar_bz2	
else
	echo "No distro selected: Nothing to do"
fi

# remove_working_dir
echo "Build Image Finished ...."
