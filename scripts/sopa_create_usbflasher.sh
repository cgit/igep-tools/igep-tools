#!/bin/bash
# sopa_create_usbflasher.sh - Script de creación automática de un pendrive asistente para la grabación de la memoria NAND de la placa SOPA0000.
# NOTA: Es la versión mejorada y más actual del script igep-media-create.sh. Este script sustituye a aquel.

# ------- Constants -------
readonly _VERSION=1.0.0
readonly _TMPDIR="$PWD/tmp-workdir"
readonly _PRECEDINGFREESPACE=8 # unidades de MBytes
readonly _BOOTPARTITIONSIZE=64 # unidades de MBytes

# ------- Private variables -------
unset _USBMEMORY _IMAGE

# ------- Private functions -------

check_parameter_value() {
    if [[ "$(echo "$2"|grep ^'\-')" ]];then
        echo "Error: la opción $1 requiere un parámetro."
        usage
    fi
}

install_necessary_packages() {
    pkgs='parted dosfstools'
    if ! dpkg -s $pkgs >/dev/null 2>&1; then
        if ! sudo apt install -y $pkgs
        then
            echo "Error: Faltan paquetes necesarios para la ejecución. Tampoco se han podido descargar de Internet."
            exit 1
        fi
    fi
}

contains() {
    [[ $list =~ (^|[[:space:]])$x($|[[:space:]]) ]] && echo 'yes' || echo 'no'
}

usage() {
    echo "usage: $(basename "$0") -d sdb -i /tmp/am335x_sopa.tar.bz2"
cat << EOF
Programa: sopa_create_usbflasher.sh

Opciones principales:

-d|--device <sdcard>
    Indica la ruta a la tarjeta SD con la que trabajar.
	* Parámetro obligatorio.
    Ejemplo: -d sdb

-i|--image <image file>
    Indica la ruta al fichero con extensión tar.bz2 donde se encuentra la imagen del sistema a grabar.
    * Parámetro obligatorio.
    Ejemplo: -i /tmp/am335x_sopa.tar.bz2

Otras opciones:
---help
    imprime este manual de ayuda.

--version
    devuelve el número de versión del script.

EOF
exit 1
}

version() {
   echo "$(basename "$0") ${_VERSION}"
cat << EOF

Copyright (C) 2020 - IATEC SL
License GPLv2: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
EOF
exit
}

parse_parameters() {
    # parse commandline options
    while [[ -n "$1" ]]; do
        case $1 in
            --help)
                usage
                ;;
            --version)
                version
                ;;
            -d|--device)
                check_parameter_value "$1" "$2"
                # Comprobar si tiene el formato sd*, por ejemplo, sdb, sdc, etc...
                re='^sd[a-z]{1}$'
                if [[ ! $2 =~ $re ]]; then
                   echo "Error: El valor asignado a la opción $1 debe ser de la forma sd*."
                   exit 1
                fi
                # Comprobar que sea un dispositivo en /dev
                if [[ ! -e /dev/$2 ]]; then
                    echo "Error: el dispositivo indicado en la opción $1 es incorrecto o no accesible."
                    exit 1
                fi
                # Comprobar que sea un dispositivo usb
                _usblist=$(lsblk -So NAME,TRAN | grep usb | awk '{print $1}')
                if [[ $(contains _usblist $2) = "no" ]]; then
                    echo "Error: el dispositivo indicado en la opción $1 parece ser que no es una tarjeta SD."
                    exit 1
                fi
                _USBMEMORY="$2"
                ;;
            -i|--image)
                check_parameter_value "$1" "$2"
                # Comprobar que tiene extensión *.tar.bz2
                re='^.*\.tar\.bz2$'
                echo $2
                if [[ ! $2 =~ $re ]]; then
                   echo "Error: El valor asignado a la opción $1 debe ser la ruta a un fichero comprimido con extensión tar.bz2."
                   exit 1
                fi
                # Comprobar que existe el fichero indicado
                if [[ ! -e $2 ]]; then
                    echo "Error: el fichero indicado en la opción $1 no existe."
                    exit 1
                fi
                _IMAGE="$2"
                ;;
        esac
        shift
    done
}

# ------- Main function --------

main() {
    install_necessary_packages

	parse_parameters "$@"

    if [[ ! $_USBMEMORY || ! $_IMAGE ]]; then
        echo "Falta uno o varios de los parámetros obligatorios."
        usage
    fi

    # desmontar todas las particiones montadas del pendrive
    echo "Desmontando la memoria USB..."
    umount /dev/${_USBMEMORY}?* &> /dev/null

    # Crear tabla de particiones (y borrar todo el contenido del pendrive)
    sudo parted -s /dev/$_USBMEMORY mklabel msdos

    # Crear la partición BOOT en FAT32
    echo "Creando la partición BOOT..."
    blocksize=$(cat /sys/block/$_USBMEMORY/queue/physical_block_size)
    firstsectorpartition1=$[$_PRECEDINGFREESPACE*1024*1024/$blocksize]
    lastsectorpartition1=$[firstsectorpartition1+$_BOOTPARTITIONSIZE*1024*1024/$blocksize]
    if ! sudo parted -s /dev/$_USBMEMORY mkpart primary fat32 ${firstsectorpartition1}s ${lastsectorpartition1}s
    then
        echo "Error: No se ha podido crear la partición BOOT en la memoria USB."
        exit 1
    fi
    sudo mkfs.vfat -F 32 -n BOOT /dev/${_USBMEMORY}1
    sudo parted -s /dev/$_USBMEMORY set 1 boot on

    # Crear la partición ROOTFS en EXT4
    echo "Creando la partición ROOTFS..."
    firstsectorpartition2=$[$lastsectorpartition1+1]
    if ! sudo parted -s /dev/$_USBMEMORY mkpart primary ext4 ${firstsectorpartition2}s 100%
    then
        echo "Error: No se ha podido crear la partición ROOTFS en la memoria USB."
        exit 1
    fi
    sudo mkfs.ext4 -F -L ROOTFS /dev/${_USBMEMORY}2

    # Guardar los cambios
    sync

    # Borrar y crear de nuevo el directorio temporal de trabajo
    if [ -e "${_TMPDIR}" ]; then
        sudo rm -fr ${_TMPDIR}
    fi
    mkdir -p ${_TMPDIR}

    echo "Llenando la partición ROOTFS..."

    # Montar la partición ROOTFS de la memoria USB
    mkdir -p ${_TMPDIR}/part2
    if ! sudo mount /dev/${_USBMEMORY}2 ${_TMPDIR}/part2
    then
        echo "Error: No se ha podido montar la partición ROOTFS de la memoria USB."
        exit 1
    fi
    # Descomprimir la imagen en la memoria USB
    if ! sudo tar -xf ${_IMAGE} -C ${_TMPDIR}/part2
    then
        echo "Error: No se ha podido descomprimir el fichero comprimido en la partición ROOTFS de la memoria USB."
        exit 1
    fi

    # Copiar la imagen completa en el rootfs
    sudo mkdir -p ${_TMPDIR}/part2/opt/firmware
    sudo cp -pL ${_IMAGE} ${_TMPDIR}/part2/opt/firmware/demo-ti-am335x-sopa.tar.bz2

    # Guardar los cambios
    sync

    echo "Llenando la partición BOOT..."

    # Montar la partición BOOT de la memoria USB
    mkdir -p ${_TMPDIR}/part1
    if ! sudo mount /dev/${_USBMEMORY}1 ${_TMPDIR}/part1
    then
        echo "Error: No se ha podido montar la partición BOOT de la memoria USB."
        exit 1
    fi
    # Crear el fichero uEnv.txt
    echo "Creando el fichero uEnv.txt"
    echo "optargs=igep-tools.auto=flash" | sudo tee ${_TMPDIR}/part1/uEnv.txt

    # Guardar los cambios
    sync

    echo "Finalizando..."

    # Borrar ficheros innecesarios de la carpeta /boot
    sudo rm -f ${_TMPDIR}/part2/boot/uEnv.txt
    sudo rm -f ${_TMPDIR}/part2/boot/u-boot.img
    sudo rm -f ${_TMPDIR}/part2/boot/MLO

    # Desmontar las particiones de la memoria USB
    sudo umount ${_TMPDIR}/part1
    sudo umount ${_TMPDIR}/part2

    # Eliminar el directorio temporal de trabajo
    sudo rm -fr ${_TMPDIR}

    echo "Proceso finalizado."
}

# -------- Execution -------------
main "$@"

exit 0
